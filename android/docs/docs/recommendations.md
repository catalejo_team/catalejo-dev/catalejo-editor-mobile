# VIM

Si eres usuario vim, estas recomendaciones te pueden servir.

* Al programar para **JAVA** el check de sintaxis *Syntastic* puede advertirte de no encontrar distintas dependencias si quieres que no esté mostrando esas advertencias ya sea porque no son de importancia para ti, puedes en el **MODO COMANDO** ejecutar  **:SyntasticToggleMode** y dejará *Syntastic* de chequear esas dependencias.

