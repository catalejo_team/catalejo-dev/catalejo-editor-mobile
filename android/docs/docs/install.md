# Cosas a instalar


## Gradle


Gradle permite ejecutar distintas tareas fuera de Android Studio.


### Lecturas

[Instalación de Gradle](https://gradle.org/install/#manually)

[Releases de Gradle](https://gradle.org/releases/)

[Could not determine java version from 11.0.2](https://stackoverflow.com/questions/54358107/gradle-could-not-determine-java-version-from-11-0-2)

[gradle cdmline inglés]()

[gradle cdmline español](https://developer.android.com/studio/build/building-cmdline?hl=ES)

### Instalación

Ejecute el script

```bash
$ ./install_gradle.sh
```

## Android Fundamentals

Recomiendo clonar el repositorio *android-fundamentals* de google que trae distintos ejemplos a tener en cuenta como ayuda.

### Actual Version

```bash
git clone https://github.com/google-developer-training/android-fundamentals-apps-v2
```

### old version

```bash
git clone https://github.com/google-developer-training/android-fundamentals.git
```

