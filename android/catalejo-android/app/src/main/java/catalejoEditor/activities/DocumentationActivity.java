package catalejoEditor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.blocklywebview.R;

import catalejoEditor.Data;

///**
// * The primary activity of the demo application. The activity embeds the
// * {@link catalejoEditor.DocWebViewFragment}.
// */
public class DocumentationActivity extends AppCompatActivity {
  WebView browser;
  // String url = "https://docs.catalejoplus.com/alternative-luabot/index.html";
  // String url = "file:///android_asset/blockly/docs/index.html";
  String url = "";
  private static final String LOG_TAG = DocumentationActivity.class.getSimpleName();
  // public static final String EXTRA_MESSAGE = "catalejoEditor.activities.extra.MESSAGE";

  /** datas **/
  private Data data = new Data();

  /*@Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.d(LOG_TAG, "catalejo-msg: construyendo actividad para Docs");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_documentation);

    browser = (WebView) findViewById(R.id.document_webview);
    final WebSettings ajustesVisorWeb = browser.getSettings();
    ajustesVisorWeb.setJavaScriptEnabled(true);

    // Intent intent = getIntent();

    // String code = intent.getStringExtra(WebAppInterface.EXTRA_MESSAGE);
    browser.loadUrl(url);
    browser.setWebViewClient(new WebViewClient());
  }*/
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_documentation);

    Intent intent = getIntent();
    data.setDataFromActivity(intent.getExtras());  // Toma los valores traidos de la Actividad Main y los asigna a la variable data

    browser=(WebView)findViewById(R.id.document_webview);

    /* Agregar características de ZOOM */
    browser.getSettings().setBuiltInZoomControls(true);
    browser.getSettings().setSupportZoom(true);

    final WebSettings ajustesVisorWeb = browser.getSettings();
    ajustesVisorWeb.setJavaScriptEnabled(true);

    browser.setWebViewClient(new WebViewClient() {

      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url+"index.html"); // url del visor actualizable,
        // en esye caso se agrega index.html porque el enrutador de mkdocs no injecta la página específicamente
        return true;
      }

    });

    // browser.loadUrl(url);
    browser.loadUrl(data.getUrlDocs()); // link de enlace a cargar al arrancar la actividad
  }

  @Override
  public void onBackPressed() {
    returnMain();
  }

  public void returnMain(){
  
    Intent returnToMain = new Intent(); // Asigna memoria para retornar datos a la Actividad Main
    data.setUrlDocs(browser.getUrl());
    Log.d(LOG_TAG, "catalejo-msg: actividad documentación: "+data.getUrlDocs());
    returnToMain.putExtras(data.getExtraBundleFromData());
    setResult(RESULT_OK, returnToMain); // Se entrega el resultado OK con el Intent
    finish(); // Se detienen la actividad Documentación
  }

}
