package catalejoEditor.activities;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blocklywebview.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;

import at.grabner.circleprogress.CircleProgressView;

import catalejoEditor.Data;

public class ConnectActivity extends AppCompatActivity {

  /** this for connection socket **/
  // Permite ejecutar operaciones asincronas evitando el bloqueo, ya que la comunicación TCP tiene un tiempo no estimado.
  // public static ConnectSocket connectSocket = null;
  // public static Socket socket = new Socket(); // Permite el manejo de sockets TCP/IP

  /** datas **/
  private Data data = new Data();
  Map<String, String> connEvent = new HashMap<String, String>();
  Map<String, String> helpOption = new HashMap<String, String>();

  /**TEXT**/
  TextView ipText;    //permite entrar y visualizar el valor de la ip
  TextView portText;  //permite entrar y visualizar el valor del puerto
  TextView textInfo;  //Da información adicional de la acción realizada en actividad conexión

  /**BUTTONS**/
  Button returnButton; //Botón que permite retornar a la actividad principal
  ImageButton sendButton; // Enviar programa a la tarjeta
  ImageButton removeButton; // Remueve el programa almacenado en la tarjeta
  ImageButton resetButton; // Reiniciar la tarjeta por software
  ImageButton playButton; // Por software solicita ejecutar el programa almacenado

  /**CIRCLE PROGRESS**/
  CircleProgressView mCircleView;

  /** PROGRESS BAR **/
  ProgressBar pgsBar;

  private static final String LOG_TAG = ConnectActivity.class.getSimpleName(); // Registro para la actividad instanciada
  @Override
  protected void onCreate(Bundle savedInstanceState) {  //Creación de la actividad
    Log.d(LOG_TAG,"catalejo-msg: construyendo actividad para Connect");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_connect);

    Intent intent = getIntent();
    data.setDataFromActivity(intent.getExtras());  // Toma los valores traidos de la Actividad Main y los asigna a la variable data
    Log.d(LOG_TAG, "catalejo-msg: Obteniendo código traído del Intent: "+data.getCode());

    /** TextView **/
    ipText = findViewById(R.id.ipText);
    portText = findViewById(R.id.portText);
    textInfo = findViewById(R.id.textInfo);

    // Evita que el teclado se lance automáticamente
    this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    /** update Ip Port in TextView**/
    putIpPortInTextView();

    /** LISTENER BUTTONS **/
    addListenerOnReturnButton();
    addListenerOnSendButton();
    addListenerOnRemoveButton();
    addListenerOnRestartButton();
    // addListenerOnPlayButton(); // Debe agregar el elmento al displa para activar esta función

    /**LISTENER PROGRESS VIEW**/
    addListenerOnCircleProgress();
    // addListenerProgressBar();
    /** CONFIG MAPS **/
    putInfoInMaps();
  }

  /** LISTENER RETURN BUTTON  **/
  public void addListenerOnReturnButton(){
    returnButton = findViewById(R.id.returnButton);
    returnButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        returnMain();
      }
    });
  }
  public void returnMain(){
    Intent returnToMain = new Intent(); // Asigna memoria para retornar datos a la Actividad Main
    data.setDataIPAndPort(ipText, portText); // Trae los datos puestos en los TextView
    returnToMain.putExtras(data.getExtraBundleFromData());
    setResult(RESULT_OK, returnToMain); // Se entrega el resultado OK con el Intent
    finish(); // Se detienen la actividad ConnectActivity
  }
  // Si presiona el botón de retornar
  @Override
  public void onBackPressed() {
    returnMain();
  }

  /** LISTENER SEND BUTTON  **/
  public void addListenerOnSendButton(){
    sendButton = findViewById(R.id.sendButton);
    sendButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        sendCode();
      }
    });
  }
  public void sendCode(){
    setProcessInformation(helpOption.get("send"));

    Log.d(LOG_TAG, "catalejo-msg: Inicia enviar código sendCode()");
    this.data.setDataIPAndPort(ipText, portText); // Actualiza data desde los TextView
    // ConnectSocket connectSocket = new ConnectSocket();
    // connectSocket.execute(this.data.getIp(), this.data.getPort(), this.data.getCode());
    // A continuación se crea la actividad asincrona de manera anónima para que apenas deje de funcionar se desvincule
    String data2send = "";
    String[] dataSplit = this.data.getCode().split("\n");
    for( int i = 0; i < dataSplit.length; i++){
      data2send += "save__data" + dataSplit[i]+"\n";
    }
    // Originalmente solo se envía el programa para ser guardado en la flash, para ejecutar el programa se deberá
    // oprimir reset en la tarjeta
    // data2send = "save__begin\n"+data2send+"save__end\n";
    // TODO 1.1 En este modo terminada el guardado del programa se procede a reiniciar al tarjeta, experimental
    data2send = "save__begin\n"+data2send+"save__end\nsave__rs\n";
    /** RESTART CIRCLE VIEW **/
    mCircleView.setRimColor(Color.YELLOW);
    mCircleView.setValue(0);
    //Log.d(LOG_TAG, "catalejo-msg: "+data2send);
    new ConnectSocket().execute(this.data.getIp(), this.data.getPort(), data2send);
  }

  /** LISTENER PLAY BUTTON  **/
  public void addListenerOnPlayButton(){
    /**
    playButton = findViewById(R.id.playButton);
    playButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        playCode();
      }
    });
     **/
  }
  public void playCode(){
    setProcessInformation(helpOption.get("play"));

    Log.d(LOG_TAG, "catalejo-msg: Inicia enviar código playCode()");
    this.data.setDataIPAndPort(ipText, portText); // Actualiza data desde los TextView
    // ConnectSocket connectSocket = new ConnectSocket();
    // connectSocket.execute(this.data.getIp(), this.data.getPort(), this.data.getCode());
    // A continuación se crea la actividad asincrona de manera anónima para que apenas deje de funcionar se desvincule
    /*String data2send = "";
    String[] dataSplit = this.data.getCode().split("\n");
    for( int i = 0; i < dataSplit.length; i++){
      data2send += "save__data" + dataSplit[i]+"\n";
    }
    data2send = "save__begin\n"+data2send+"save__end\n";
    */
    /** RESTART CIRCLE VIEW **/
    mCircleView.setRimColor(Color.YELLOW);
    mCircleView.setValue(0);

    //Log.d(LOG_TAG, "catalejo-msg: "+data2send);
    //new ConnectSocket().execute(this.data.getIp(), this.data.getPort(), data2send);
    new ConnectSocket().execute(this.data.getIp(), this.data.getPort(), "save__play\n");
  }

  /** LISTENER REMOVE BUTTON  **/
  public void addListenerOnRemoveButton(){
    removeButton = findViewById(R.id.removeButton);
    removeButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        removeLuaFileInLuaBot();
      }
    });
  }
  public void removeLuaFileInLuaBot(){  //Remueve el archivo run.lua en el luabot
    setProcessInformation(helpOption.get("remove"));

    Log.d(LOG_TAG, "catalejo-msg: Inicia enviar código removeLuaFileInLuaBot()");
    this.data.setDataIPAndPort(ipText, portText); // Actualiza data desde los TextView
    // ConnectSocket connectSocket = new ConnectSocket();
    // connectSocket.execute(this.data.getIp(), this.data.getPort(), this.data.getCode());
    // A continuación se crea la actividad asincrona de manera anónima para que apenas deje de funcionar se desvincule
    String data2send = "";
    data2send = "save__rm\n";
    /** RESTART CIRCLE VIEW **/
    mCircleView.setRimColor(Color.YELLOW);
    mCircleView.setValue(0);
    new ConnectSocket().execute(this.data.getIp(), this.data.getPort(), data2send);
  }

  /** LISTENER RESTART BUTTON  **/
  public void addListenerOnRestartButton(){
    resetButton = findViewById(R.id.resetButton);
    resetButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        restartLuaBot();
      }
    });
  }
  public void restartLuaBot(){  //Remueve el archivo run.lua en el luabot
    setProcessInformation(helpOption.get("reset"));

    Log.d(LOG_TAG, "catalejo-msg: Inicia enviar código restartLuaBot()");
    this.data.setDataIPAndPort(ipText, portText); // Actualiza data desde los TextView
    // ConnectSocket connectSocket = new ConnectSocket();
    // connectSocket.execute(this.data.getIp(), this.data.getPort(), this.data.getCode());
    // A continuación se crea la actividad asincrona de manera anónima para que apenas deje de funcionar se desvincule
    String data2send = "";
    data2send = "save__rs\n";
    /** RESTART CIRCLE VIEW **/
    mCircleView.setRimColor(Color.YELLOW);
    mCircleView.setValue(0);
    new ConnectSocket().execute(this.data.getIp(), this.data.getPort(), data2send);
  }

  /**LISTENER CIRCLE PROGRESS**/
  public void addListenerOnCircleProgress(){
    mCircleView = findViewById(R.id.sendProgressBar);
    mCircleView.setTextSize(45);
    mCircleView.setTextColor(Color.WHITE);
    mCircleView.setUnit("%");
    mCircleView.setUnitColor(Color.WHITE);
    mCircleView.setUnitSize(25);
    mCircleView.setUnitVisible(true);
    mCircleView.setBarColor(Color.GREEN);
    mCircleView.setRimColor(Color.GRAY);
    mCircleView.setValue(0);
    mCircleView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
      @Override
      public void onProgressChanged(float value) {
        Log.d(LOG_TAG, "Progress Changed: " + value);
      }
    });
  }

  /**LISTENER PROGRESS BAR**/
  public void addListenerProgressBar(){
    pgsBar = findViewById(R.id.sendProgressBar);
    /*mCircleView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
      @Override
      public void onProgressChanged(float value) {
        Log.d(LOG_TAG, "Progress Changed: " + value);
      }
    });*/
    pgsBar.setVisibility(View.INVISIBLE);
    //pgsBar.setMax(100);
    pgsBar.setVisibility(View.VISIBLE);
  }

  /** actualizar datos de ip y port en el TextView **/
  private void putIpPortInTextView(){
    ipText.setText(data.getIp());
    portText.setText(Integer.toString(data.getPort()));
  }

  /** PRIVATE CLASS CONNECT SOCKET **/
  private  class ConnectSocket extends AsyncTask<Object, Integer, String[]> {
  // private class ConnectSocket extends AsyncTask<Object, Void, String> {

    private Socket socket = new Socket();
    /*
     * Los parámetros de entrada son:
     *  String ip -> params[0]
     *  int port -> params[1]
     */

    // Refactorizar código
    @Override
    protected String[] doInBackground(Object... params) {
      String ip = (String) params[0];
      int port = (int) params[1];
      String code = (String) params[2];
      SocketAddress address = new InetSocketAddress(ip, port);
      // socket = null;
      // socket = new Socket();
      try {
        socket.connect(address, 10000);
        publishProgress(0, Color.BLUE); // GREEN
      } catch(Exception e) {
        publishProgress(0, Color.RED); // RED
        //Toasty.error(getApplicationContext(), "No pudo establecer conexion con la tarjeta", Toast.LENGTH_LONG).show();
        //Toast.makeText(getApplicationContext(), "No se pudo establecer conexión con la tarjeta", Toast.LENGTH_LONG).show();
        //return "It is no possible to socket connect";
        return new String[] {"dontConn", "err"};
      }
      Log.d(LOG_TAG, "catalejo-msg conectado a IP: "+ip+", PORT: "+port);


      try {
        BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        try {
          PrintWriter salida = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
          if(entrada.readLine() == "luabot"){

          }
          // Split code
          String[] codeSplit = code.split("\n");
          for( int i = 0; i < codeSplit.length; i++){
            try {
              //salida.print(codeSplit[i]);     // no agrega nada adicional
              salida.println(codeSplit[i]); //termina con una nueva línea
              // Aún no aparecen en el log las respuestas del server, sin embargo, hay que revisar si lo hace también con el tcp.
              try{
                String temp = entrada.readLine();

                if(temp !=null) {
                  Log.d(LOG_TAG, "catalejo-msg: " + (String) temp);
                  // TODO 1.1 Se debería iniciar en 1, se pone en 2 para que se pueda reiniciar la tarjeta apenas se guarde el código
                  publishProgress((i+2)*100/codeSplit.length, Color.BLUE);
                }else {
                  Log.d(LOG_TAG, "catalejo-msg: temp vacio");
                  return new String[] {"wrongAnswer", "badAnswer"};
                }
              }catch (Exception e){
                Log.d(LOG_TAG, "catalejo-msg: "+e.toString());
                return new String[] {"confirCodeLost", "badAnswer"};
              }
            }catch(Exception e){
              Log.d(LOG_TAG, "catalejo-msg: "+e.toString());
              return new String[] {"loseConn", "badAnswer"};
            }
          }
        } catch(Exception e) {
          //pass
          return new String[] {"dontRespCommads", "err"};
        }
      } catch(Exception e) {
        //pass
        return new String[] {"desConn", "err"};
      }
      try {
        socket.close();
        Log.d(LOG_TAG, "catalejo-msg: Se ha cerrado el socket");
      } catch(Exception e) {
      }

      return new String[] {"ok", "rst"};
    }

    @Override
    protected void onProgressUpdate(Integer... values){
      mCircleView.setRimColor(values[1]);
      mCircleView.setValueAnimated(values[0]);
    }

    @Override
    protected void onPostExecute(String[] result){
      try {
        socket.close();
        Log.d(LOG_TAG, "catalejo-msg: Se ha cerrado el socke.gett");
        Toast.makeText(getApplicationContext(), connEvent.get(result[0]), Toast.LENGTH_LONG).show();
        if(result[1] != null){
          setProcessInformation(helpOption.get(result[1]));
        }
        // mCircleView.setValueAnimated(10);
      } catch(Exception e) {
      }
    }
  }

  protected void setProcessInformation(String info){
    textInfo.setText(info);
  }

  protected void putInfoInMaps(){
    connEvent.put("ok", "¡Súper! La tarjeta a realizado la tarea que le pediste.");
    connEvent.put("dontConn", "No fue posible establecer conexión con la tarjeta.");
    connEvent.put("desConn", "Se ha desconectado de la tarjeta, verifica que esté encendida.");
    connEvent.put("dontRespCommads", "La tarjeta no responde a los comandos, comprueba que te estás conectando a la tarjeta y no a otro aparato.");
    connEvent.put("loseConn", "Se ha perdido la conexión con la tarjeta, intenta de nuevo.");
    connEvent.put("confirCodeLost", "La tarjeta no envía código de confirmación.");
    connEvent.put("wrongAnswer", "La tarjeta no responde correctamente, intenta enviar de nuevo el programa.");

    helpOption.put("rst", "¡Hecho!\nSi enviaste el programa a la tarjeta nodeMCU no olvides reiniciarla oprimiendo en la tarjeta el botón RST.\n"+
      "Ten presente que en el reinicio tu celular se puede desconectar de la tarjeta, revisa la conexión WiFi antes de enviar una nueva orde.\n");
    helpOption.put("err", "Lo sentimos, pero no fue posible comunicarse con la tarjeta.\n"+
      "Te sugerimos revisar que la tarjeta esté encendida y que tú estés conectada a ella a través de WiFi.\n");
    helpOption.put("badAnswer", "Si estás reiniciando la tarjeta, quiere decir que ésta es la respuesta correcta; "+
            "revisa tu conexión WiFi y de ser necesario vuelve a conectarte a la tarjeta. \n\n"+
            "Si no estabas reiniciando la tarjeta es posible que haya \"ruido electromagnetico\" intenta enviando nuevamente el programa,"+
      "además, considera hacerlo en otro lugar que mejore la comunicación.\n");
    helpOption.put("send", "Procesando la orden de enviar el programa a la tarjeta, por favor espera.\n");
    helpOption.put("play", "Procesando la orden de iniciar el programa a la tarjeta, por favor espera.\n");
    helpOption.put("remove", "Procesando la orden de remover el programa a la tarjeta, por favor espera.\n");
    helpOption.put("reset", "Procesando la orden de reiniciar la tarjeta; por favor espera.\n"+
      "Ten presente que el celular se puede desconectar mientras hace este proceso,"+
      "revisa la conexión antes de enviar otra orden.\n");
  }
}
