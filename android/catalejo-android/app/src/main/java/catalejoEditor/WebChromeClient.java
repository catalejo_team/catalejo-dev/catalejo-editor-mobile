package catalejoEditor;

// import android.util.Log;
// import android.webkit.ConsoleMessage;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebView;

/**
 * Provides native hooks for JavaScript console dialog functions.
 */
public class WebChromeClient extends android.webkit.WebChromeClient {
    @Override
    public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        new JsDialogHelper(result, JsDialogHelper.ALERT, null, message, url)
                .showDialog(view.getContext());
        return true;
    }

    @Override
    public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
        new JsDialogHelper(result, JsDialogHelper.CONFIRM, null, message, url)
                .showDialog(view.getContext());
        return true;
    }

    @Override
    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue,
            JsPromptResult result) {
        new JsDialogHelper(result, JsDialogHelper.PROMPT, defaultValue, message, url)
                .showDialog(view.getContext());
        return true;
    }

    // AGREGAR LA CONSOLA DE JAVA_SCRIPT AL WEBVIEW
    // @Override
    // public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
    //     Log.d("MyApplication", "catalejo-msg:" + consoleMessage.message() + " -- From line " +
    //     consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
    //     return true;
    // }
}
