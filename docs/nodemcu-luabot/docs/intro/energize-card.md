# Energizar tarjeta

## Energizar tarjeta con powerbank

![materiales](../img/energize-card/powerbank.jpg)


<iframe width="560" height="315" src="https://www.youtube.com/embed/O9mnupLpM-0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Energizar tarjeta con baterías

![pilasAAA](../img/energize-card/pilasAAA.jpg)

![Conexión correcta](../img/energize-card/correcta-conexion.jpg)


<iframe width="560" height="315" src="https://www.youtube.com/embed/faTTyd7eBFI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
