# Catalejo-firmware

Queremos señalar que ésta tarea requiere seguir los pasos de instalación
según el sistema operativo que tengas en tu computador, ya sea GNU/Linux,
IOS o Windows.

## Instalación desde Windows

### 1. Instalación de drivers

#### cp2102

[Link página](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)

#### ch340

[Link página](http://www.wch.cn/download/CH341SER_ZIP.html)


### 2. Descargar catalejo-firmware.bin

[Link de descarga del fimrware](https://sourceforge.net/projects/catalejo-editor-luabot/files/catalejo-firmware.bin)

### 3. Descargar nodemcu-pyflasher

[NodeMCU-PyFlasher 64bits](https://github.com/marcelstoer/nodemcu-pyflasher/releases/download/v4.0/NodeMCU-PyFlasher-4.0-x64.exe)

[NodeMCU-PyFlasher 32Bits](https://github.com/marcelstoer/nodemcu-pyflasher/releases/download/v4.0/NodeMCU-PyFlasher-4.0-x86.exe)

!!! info
    Si tiene problemas para usar la aplicación NodeMCU-PyFlasher puedes ir a la sección
    de [liberaciones](https://github.com/marcelstoer/nodemcu-pyflasher/releases) y buscar alguna anterior para que pruebes con una que funcione con
    tu sistema operativo.

### 4. Instalación del firmware

## Instalación desde Mac

### 1. Instalación de drivers

#### cp2102

[Link página](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)

#### ch340

[Link página](http://www.wch.cn/download/CH341SER_ZIP.html)


### 2. Descargar catalejo-firmware.bin

[Link de descarga del fimrware](https://sourceforge.net/projects/catalejo-editor-luabot/files/catalejo-firmware.bin)

### 3. Descargar nodemcu-pyflasher

[NodeMCU-PyFlasher-4.0.dmg](https://github.com/marcelstoer/nodemcu-pyflasher/releases/download/v4.0/NodeMCU-PyFlasher-4.0.dmg)

### 4. Instalación del firmware

## Instalación desde Linux

### 1. Instalación de drivers

**Nota**: Para linux los drivers generalmente vienen instalados

### 2. Descargar catalejo-firmware.bin

[Link de descarga del fimrware](https://sourceforge.net/projects/catalejo-editor-luabot/files/catalejo-firmware.bin)

### 3. Descargar nodemcu-pyflasher

Lanza una terminal de GNU/Linux (shell) y ejecuta el siguiente comando de instalación
```bash
sudo apt install esptool
```

### 4. Instalación del firmware

```bash
esptool.py write_flash 0x00000 catalejo-firmware.bin
```
