# Sensor de humedad del suelo

## Ejemplo de conexión

![Esquema humedad del suelo](../img/sensores/soil_moisture/nodemcu_humedad_suelo_bb.png)

## Ejemplo de algoritmo

A continuación te presentamos dos maneras de escribir el mismo algoritmo.

En este algoritmo usarás las nociones de la ecuación de la recta para parametrizar el sensor.

![Humedad del suelo](../img/sensores/soil_moisture/soil_moisture.png)

En éste algoritmo usamos la función de *cambiar rango* donde ella automáticamente determinará
cual será el valor de la medición en la nueva escala.

![algoritmo 2](../img/sensores/soil_moisture/suelo-algoritmo.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

