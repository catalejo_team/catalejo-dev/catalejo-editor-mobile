# Sensor de lluvia

Este sensor puede ser usado para determinar la cantidad de agua en un recipiente.
Si por ejemplo queremos medir la cantidad de lluvia podemos construir nuestro
propio pluviómetro acondicionando un recipiente para tal propósito y protegiendo
del sensor los elementos electrónicos.

## Ejemplo

### Conexiones

![conexiones](../img/sensores/rainSensor/nodemcu_rainSensor_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

### Algoritmo

![algortimo](../img/sensores/rainSensor/lluvia-algoritmo.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

