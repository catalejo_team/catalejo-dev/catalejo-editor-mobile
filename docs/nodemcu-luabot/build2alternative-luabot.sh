#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	exportar documentación alternativa para ser usada por un webserver
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Wednesday 28 October 2020
status=$?

BUILD=./build/*
REPOSITORY=~/projects/alternative-luabot/

rm -rf $REPOSITORY*
cp -ra $BUILD $REPOSITORY 
