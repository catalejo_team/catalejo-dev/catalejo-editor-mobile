# Catalejo editor móvil

![luabot](./img/luabot.jpeg)


Esta aplicación permite la programación de placas de desarrollo basadas en
micropython o nodeMCU; el firmware de la placa también es acondicionado
para mejorar las posibilidades en la experiencia de programación.

Sus principales características:

* La creación del código es desarrollada directamente desde un dispositivo Android 5.0+
* La programación de la tarjeta se puede realizar de manera inalámbrica
* El código a desarrollar por parte del usuario es implementado en blockly de google.
* El uso de blockly permite que el usuario pueda concentrarse en el algoritmo a desarrollar
conservando la flexibilidad de los lenguajes de programación
* El uso de micropython o nodeMCU como firmware tienen la ventaja de no requerir un
compilador por parte del usuario
* Las aplicaciones a desarrollar pueden hacer parte de IOT o control remoto por peticiones
HTTP

En la primera versión se hace soporte del kit de desarrollo [Luabot](https://bitbucket.org/pinguinotux/luabot/src/master/).
Luabot consiste de un entorno de programación para escritorio basado Chrome y una tarjeta de desarrollo creada a partir
del esp8266-07 que corre un interprete Lua. Se conserva la idea de programación y además se da soporte a la placa de desarrollo
[nodeMCU V3](https://github.com/nodemcu/nodemcu-devkit-v1.0).

## Documentación y comunidades

Si necesitas ayudas o quieres compartir tus experiencias con el kit, te invitamos a que hagas parte
de nuestra comunidad accediendo a los siguientes links:

[Documentación de usuario](https://docs.catalejoplus.com/kits/alternative-luabot/)

[Servidor de discord](https://discord.gg/4GxYxyy)

[Foro de flarum](https://catalejo.flarum.cloud/t/catalejo-luabot-nodemcu)

## Instalación

//TODO

## Licencia

El código publicado en este repositorio es Open Source y tiene licencias:

* Apache 2.0
* CC 4.0
* MIT

Esto es debido a que el desarrollo de éste proyecto consta de varias módulos
que tienen sus propias licencias que deben ser respetadas.

## Colaboradores

Agradecimiento muy especial a cada uno de los integrantes del equipo catalejo
que desde sus experiencias han aportado con observaciones o sugerencias y que
siempre creyeron en el proyecto:

* [Carlos Camargo](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000006149)
* Carolina Pulido
* William Ruíz
* Felipe Polanco
* Ana Pulido

Agradecimiento muy especial a cada uno de los estudiantes que hizo uso de esta
plataforma en sus experiencias de aprendizaje y nos demostró que era posible
seguir creciendo.

Atentamente,
<div>
<a  href="https://github.com/johnnycubides"><b>Johnny Cubides</b></a><br>
<img src="./img/catalejo-light.png" alt="./img/catalejo-light.png" style="width:5%;" width="5%"><br>
</div>


## Donaciones 

<a href="https://www.buymeacoffee.com/johnnycubides"><img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=johnnycubides&button_colour=BD5FFF&font_colour=ffffff&font_family=Cookie&outline_colour=000000&coffee_colour=FFDD00"></a>
