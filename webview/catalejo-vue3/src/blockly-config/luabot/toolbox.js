export const toolbox = 
`
<xml id="toolbox" style="display: none">
  <category name="Lógica" colour="%{BKY_LOGIC_HUE}">
    <block type="controls_if"></block>
    <block type="logic_compare"></block>
    <block type="logic_operation"></block>
    <block type="logic_negate"></block>
    <block type="logic_boolean"></block>
    <block type="logic_null"></block>
    <block type="logic_ternary"></block>
  </category>
  <category name="Secuencias" colour="%{BKY_LOOPS_HUE}">
    <block type="controls_repeat_ext">
      <value name="TIMES">
        <shadow type="math_number">
          <field name="NUM">10</field>
        </shadow>
      </value>
    </block>
    <block type="controls_whileUntil"></block>
    <block type="controls_for">
      <value name="FROM">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
      <value name="TO">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="controls_forEach"></block>
    <block type="controls_flow_statements"></block>
  </category>
  <category name="Matemática" colour="%{BKY_MATH_HUE}">
    <block type="math_number"></block>
    <block type="math_arithmetic">
      <value name="A">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
      <value name="B">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="math_single">
      <value name="NUM">
        <shadow type="math_number">
          <field name="NUM">9</field>
        </shadow>
      </value>
    </block>
    <block type="math_trig">
      <value name="NUM">
        <shadow type="math_number">
          <field name="NUM">45</field>
        </shadow>
      </value>
    </block>
    <block type="math_constant"></block>
    <block type="math_number_property">
      <value name="NUMBER_TO_CHECK">
        <shadow type="math_number">
          <field name="NUM">0</field>
        </shadow>
      </value>
    </block>
    <block type="math_round">
      <value name="NUM">
        <shadow type="math_number">
          <field name="NUM">3.1</field>
        </shadow>
      </value>
    </block>
    <block type="math_on_list"></block>
    <block type="math_modulo">
      <value name="DIVIDEND">
        <shadow type="math_number">
          <field name="NUM">64</field>
        </shadow>
      </value>
      <value name="DIVISOR">
        <shadow type="math_number">
          <field name="NUM">10</field>
        </shadow>
      </value>
    </block>
    <block type="math_constrain">
      <value name="VALUE">
        <shadow type="math_number">
          <field name="NUM">50</field>
        </shadow>
      </value>
      <value name="LOW">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
      <value name="HIGH">
        <shadow type="math_number">
          <field name="NUM">100</field>
        </shadow>
      </value>
    </block>
    <block type="math_random_int">
      <value name="FROM">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
      <value name="TO">
        <shadow type="math_number">
          <field name="NUM">100</field>
        </shadow>
      </value>
    </block>
    <block type="math_random_float"></block>
  </category>
  <category name="Texto" colour="%{BKY_TEXTS_HUE}">
    <block type="text"></block>
    <block type="text_join"></block>
    <block type="text_append">
      <value name="TEXT">
        <shadow type="text"></shadow>
      </value>
    </block>
    <block type="text_length">
      <value name="VALUE">
        <shadow type="text">
          <field name="TEXT">abc</field>
        </shadow>
      </value>
    </block>
    <block type="text_isEmpty">
      <value name="VALUE">
        <shadow type="text">
          <field name="TEXT"></field>
        </shadow>
      </value>
    </block>
    <block type="text_indexOf">
      <value name="VALUE">
        <block type="variables_get">
          <field name="VAR">{textVariable}</field>
        </block>
      </value>
      <value name="FIND">
        <shadow type="text">
          <field name="TEXT">abc</field>
        </shadow>
      </value>
    </block>
    <block type="text_charAt">
      <value name="VALUE">
        <block type="variables_get">
          <field name="VAR">{textVariable}</field>
        </block>
      </value>
    </block>
    <block type="text_getSubstring">
      <value name="STRING">
        <block type="variables_get">
          <field name="VAR">{textVariable}</field>
        </block>
      </value>
    </block>
    <block type="text_changeCase">
      <value name="TEXT">
        <shadow type="text">
          <field name="TEXT">abc</field>
        </shadow>
      </value>
    </block>
    <block type="text_trim">
      <value name="TEXT">
        <shadow type="text">
          <field name="TEXT">abc</field>
        </shadow>
      </value>
    </block>
    <block type="text_print">
      <value name="TEXT">
        <shadow type="text">
          <field name="TEXT">abc</field>
        </shadow>
      </value>
    </block>
    <block type="text_prompt_ext">
      <value name="TEXT">
        <shadow type="text">
          <field name="TEXT">abc</field>
        </shadow>
      </value>
    </block>
  </category>
  <category name="Listas" colour="%{BKY_LISTS_HUE}">
    <block type="lists_create_with">
      <mutation items="0"></mutation>
    </block>
    <block type="lists_create_with"></block>
    <block type="lists_repeat">
      <value name="NUM">
        <shadow type="math_number">
          <field name="NUM">5</field>
        </shadow>
      </value>
    </block>
    <block type="lists_length"></block>
    <block type="lists_isEmpty"></block>
    <block type="lists_indexOf">
      <value name="VALUE">
        <block type="variables_get">
          <field name="VAR">{listVariable}</field>
        </block>
      </value>
    </block>
    <block type="lists_getIndex">
      <value name="VALUE">
        <block type="variables_get">
          <field name="VAR">{listVariable}</field>
        </block>
      </value>
    </block>
    <block type="lists_setIndex">
      <value name="LIST">
        <block type="variables_get">
          <field name="VAR">{listVariable}</field>
        </block>
      </value>
    </block>
    <block type="lists_getSublist">
      <value name="LIST">
        <block type="variables_get">
          <field name="VAR">{listVariable}</field>
        </block>
      </value>
    </block>
    <block type="lists_split">
      <value name="DELIM">
        <shadow type="text">
          <field name="TEXT">,</field>
        </shadow>
      </value>
    </block>
    <block type="lists_sort"></block>
  </category>
  <sep></sep>
  <category name="Variables" colour="%{BKY_VARIABLES_HUE}" custom="VARIABLE"></category>
  <category name="Funciones" colour="%{BKY_PROCEDURES_HUE}" custom="PROCEDURE"></category>
  <category name="Mis Funciones" colour="%{BKY_PROCEDURES_HUE}">
    <block type="function_map"></block>
    <block type="function_map_return">
      <value name="x">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
      <value name="x1">
        <shadow type="math_number">
          <field name="NUM">0</field>
        </shadow>
      </value>
      <value name="y1">
        <shadow type="math_number">
          <field name="NUM">0</field>
        </shadow>
      </value>
      <value name="x2">
        <shadow type="math_number">
          <field name="NUM">1023</field>
        </shadow>
      </value>
      <value name="y2">
        <shadow type="math_number">
          <field name="NUM">100</field>
        </shadow>
      </value>
    </block>
  </category>
  <sep></sep>
  <category id="myPines" name="Pines" colour="210">
    <block type="pin_d"></block>
    <block type="gpio_mode">
      <value name="pin">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="gpio_write">
      <value name="pin">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="gpio_write_variable"></block>
    <block type="gpio_read">
      <value name="pin">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="gpio_write_value">
      <value name="pin value">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="gpio_write_pin_value">
      <value name="pin name">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
      <value name="pin value">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="gpio_read_variable"></block>
    <block type="gpio_trig">
      <value name="pin">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="gpio_trig_var"></block>
    <block type="gpio_trig_level"></block>
    <block type="gpio_trig_when"></block>
    <block type="gpio_trig_compare"></block>
    <block type="adc_read"></block>
  </category>
  <category id="myTmrs" name="Temporizadores" colour="65">
    <block type="tmr_var">
      <value name="time">
        <shadow type="math_number">
          <field name="NUM">1</field>
        </shadow>
      </value>
    </block>
    <block type="alarm_create">
      <value name="interval">
        <shadow type="math_number">
          <field name="NUM">1000</field>
        </shadow>
      </value>
    </block>
    <block type="alarm_start"></block>
    <block type="alarm_stop"></block>
  </category>
  <sep></sep>
  <category id="sensors" name="Sensores" colour="180">
    <category id="DHT" name="DHT" colour="180">
      <block type="dht_read">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="dht_view"></block>
    </category>
    <category id="DS18B20" name="DS18B20" colour="180">
      <block type="ds18b20_lib"></block>
      <block type="ds18b20_read">
        <value name="Pin">
          <shadow type="math_number">
            <field name="NUM">3</field>
          </shadow>
        </value>
      </block>
    </category>
    <category id="hcst04" name="Ultrasonido" colour="180">
      <block type="hcsr04_lib"></block>
      <block type="ultrasonic_setup">
        <value name="trigger pin">
          <shadow type="math_number">
            <field name="NUM">5</field>
          </shadow>
        </value>
        <value name="echo pin">
          <shadow type="math_number">
            <field name="NUM">6</field>
          </shadow>
        </value>
      </block>
      <block type="ultrasonic_if_status_ok"></block>
      <block type="ultrasonic_get"></block>
      <block type="ultrasonic_set"></block>
      <block type="ultrasonic_status"></block>
      <block type="ultrasonic_if_status"></block>
    </category>
  </category>
  <sep></sep>
  <category id="actuadores" name="Actuadores" colour="290">
    <category id="LED" name="LED" colour="290">
      <block type="create_led">
        <value name="pin">
          <block type="pin_d"></block>
        </value>
      </block>
      <block type="led_output"></block>
    </category>
    <category id="myServo" name="Servomotor" colour="290">
      <block type="servo_init">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="angle">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="servo_angle">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="angle">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="servo_stop">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
    </category>
    <category id="ws2812" name="Ws2812" colour="290">
      <block type="ws2812_init"></block>
      <block type="ws2812_format"></block>
      <block type="ws2812_array_finish">
        <value name="R">
          <shadow type="math_number">
            <field name="NUM">255</field>
          </shadow>
        </value>
        <value name="G">
          <shadow type="math_number">
            <field name="NUM">255</field>
          </shadow>
        </value>
        <value name="B">
          <shadow type="math_number">
            <field name="NUM">255</field>
          </shadow>
        </value>
      </block>
      <block type="ws2812_array">
        <value name="R">
          <shadow type="math_number">
            <field name="NUM">255</field>
          </shadow>
        </value>
        <value name="G">
          <shadow type="math_number">
            <field name="NUM">255</field>
          </shadow>
        </value>
        <value name="B">
          <shadow type="math_number">
            <field name="NUM">255</field>
          </shadow>
        </value>
      </block>
      <block type="ws2812_write"></block>
      <block type="ws2812_write_dual"></block>
    </category>
    <category id="displayOled" name="Display" colour="290">
      <block type="display_oled_init">
        <value name="id">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="sda">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="scl">
          <shadow type="math_number">
            <field name="NUM">2</field>
          </shadow>
        </value>
        <value name="sla">
          <shadow type="math_number">
            <field name="NUM">60</field>
          </shadow>
        </value>
      </block>
      <block type="display_oled_buffer"></block>
      <block type="display_oled_angle_text"></block>
      <block type="display_oled_str">
        <value name="text">
          <shadow type="text">
            <field name="TEXT">Hola</field>
          </shadow>
        </value>
        <value name="x0">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="y0">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="display_oled_circ_disc">
        <value name="x0">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
        <value name="y0">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
        <value name="radio">
          <shadow type="math_number">
            <field name="NUM">9</field>
          </shadow>
        </value>
      </block>
      <block type="display_oled_frame_box">
        <value name="x0">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="y0">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="width">
          <shadow type="math_number">
            <field name="NUM">20</field>
          </shadow>
        </value>
        <value name="height">
          <shadow type="math_number">
            <field name="NUM">20</field>
          </shadow>
        </value>
      </block>
      <block type="display_oled_line">
        <value name="x0">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="y0">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="x1">
          <shadow type="math_number">
            <field name="NUM">30</field>
          </shadow>
        </value>
        <value name="y1">
          <shadow type="math_number">
            <field name="NUM">30</field>
          </shadow>
        </value>
      </block>
    </category>
    <category name="PWM" colour="290">
      <block type="pwm_setup">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">4</field>
          </shadow>
        </value>
        <value name="frec">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
        <value name="duty">
          <shadow type="math_number">
            <field name="NUM">512</field>
          </shadow>
        </value>
      </block>
      <block type="pwm_start">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">4</field>
          </shadow>
        </value>
      </block>
      <block type="pwm_stop">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">4</field>
          </shadow>
        </value>
      </block>
      <block type="pwm_change_duty">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">4</field>
          </shadow>
        </value>
        <value name="duty">
          <shadow type="math_number">
            <field name="NUM">512</field>
          </shadow>
        </value>
      </block>
      <block type="pwm_change_frecuency">
        <value name="pin">
          <shadow type="math_number">
            <field name="NUM">4</field>
          </shadow>
        </value>
        <value name="frec">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
    </category>
  </category>
  <sep></sep>
  <category id="iot" name="IOT" colour="255">
    <category id="html" name="HTML" colour="255">
      <block type="html_page"></block>
      <block type="html_button_view"></block>
      <block type="html_button_logic"></block>
      <block type="html_text"></block>
      <block type="html_text_add_variable"></block>
      <block type="html_text_size">
        <value name="size">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
    </category>
    <category id="thingspeak" name="Thingspeak" colour="255">
      <block type="thingspeak"></block>
      <block type="thingspeak_send"></block>
      <block type="thingspeak_field"></block>
    </category>
    <category id="Sockets" name="Sockets" colour="255">
      <block type="init_tcp">
        <value name="timeout">
          <shadow type="math_number">
            <field name="NUM">30</field>
          </shadow>
        </value>
      </block>
      <block type="tcp_listen">
        <value name="var_port">
          <shadow type="math_number">
            <field name="NUM">3000</field>
          </shadow>
        </value>
      </block>
      <block type="tcp_events"></block>
      <block type="tcp_data"></block>
      <block type="tcp_send"></block>
    </category>
  </category>
  <sep></sep>
  <category id="sound" name="Sonido" colour="160">
    <category id="osc" name="OSC" colour="160">
      <block type="service_osc_init"></block>
      <block type="service_osc_send">
        <value name="port">
          <shadow type="math_number">
            <field name="NUM">6449</field>
          </shadow>
        </value>
        <value name="ip">
          <shadow type="text">
            <field name="TEXT">192.168.1.1</field>
          </shadow>
        </value>
      </block>
      <block type="osc_format_iiff">
        <value name="address">
          <shadow type="text">
            <field name="TEXT">/piano</field>
          </shadow>
        </value>
        <value name="int1">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="int2">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="float1">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="float2">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="osc_format_ifs">
        <value name="address">
          <shadow type="text">
            <field name="TEXT">/piano</field>
          </shadow>
        </value>
        <value name="integer">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="float">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
        <value name="text">
          <shadow type="text">
            <field name="TEXT"></field>
          </shadow>
        </value>
      </block>
      <block type="service_osc_listen">
        <value name="port">
          <shadow type="math_number">
            <field name="NUM">6449</field>
          </shadow>
        </value>
      </block>
      <block type="osc_in_data"></block>
    </category>
    <category id="midi" name="MIDI" colour="160">
      <block type="midi"></block>
    </category>
  </category>
  <sep></sep>
  <category id="comm" name="Comunicaciones" colour="265">
    <category id="wifi" name="WIFI" colour="255">
      <block type="wifi_sta">
        <value name="redName">
          <shadow type="text">
            <field name="TEXT">miRed</field>
          </shadow>
        </value>
        <value name="password">
          <shadow type="text">
            <field name="TEXT">contrasena</field>
          </shadow>
        </value>
      </block>
      <block type="wifi_ap">
        <value name="redName">
          <shadow type="text">
            <field name="TEXT">miRed</field>
          </shadow>
        </value>
        <value name="password">
          <shadow type="text">
            <field name="TEXT">contrasena</field>
          </shadow>
      </value>
      <value name="channel">
        <shadow type="math_number">
          <field name="NUM">11</field>
        </shadow>
      </value>
      </block>
      <block type="wifi_ap_sta">
        <value name="redNameAP">
          <shadow type="text">
      <field name="TEXT">miRed</field>
          </shadow>
        </value>
        <value name="passwordAP">
          <shadow type="text">
      <field name="TEXT">contrasena</field>
          </shadow>
      </value>
      <value name="channel">
        <shadow type="math_number">
          <field name="NUM">11</field>
        </shadow>
      </value>
        <value name="redNameSTA">
          <shadow type="text">
      <field name="TEXT">miRed</field>
          </shadow>
        </value>
        <value name="passwordSTA">
          <shadow type="text">
      <field name="TEXT">contrasena</field>
          </shadow>
        </value>
      </block>
    </category>
  </category>
</xml>
`