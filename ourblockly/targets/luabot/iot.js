/*
 * start thingspeak
 */

Blockly.Blocks['thingspeak'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Start ThingSpeak");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ts.gif", 100, 25, "*"));
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['thingspeak'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = 'function sendData(apikey, datas)\n'+
'print("Sending data to thingspeak.com")\n'+
'conn=net.createConnection(net.TCP, 0)\n'+
'conn:on("receive", function(conn, payload) print(payload) end)\n'+
'conn:connect(80,\'184.106.153.149\')\n'+
'conn:send("GET /update?key="..apikey..datas.." HTTP/1.1\\r\\n")\n'+
'conn:send("Host: api.thingspeak.com\\r\\n")\n'+
'conn:send("Accept: */*\\r\\n")\n'+
'conn:send("User-Agent: Mozilla/4.0 (compatible; esp8266 Lua; Windows NT 5.1)\\r\\n")\n'+
'conn:send("\\r\\n")\n'+
'conn:on("sent",function(conn)\n'+
'print("Closing connection")\n'+
'conn:close()end)\n'+
'conn:on("disconnection", function(conn)\n'+
'print("Got disconnection...")\n'+
'end)end\n';
  return code;
};

/*
 * thinspeak send
 */
 
Blockly.Blocks['thingspeak_send'] = {
  init: function() {
    this.appendValueInput("key")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ts.gif", 70, 20, "*"))
        .appendField("API Key");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Escribir variables a ThingSpeak");
    this.appendStatementInput("datas")
        .setCheck(null);
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['thingspeak_send'] = function(block) {
  var value_key = Blockly.Lua.valueToCode(block, 'key', Blockly.Lua.ORDER_ATOMIC);
  var statements_datas = Blockly.Lua.statementToCode(block, 'datas');
  // TODO: Assemble Lua into code variable.
  var code = 'myDatas = ""'+ statements_datas+'\n'+
	'sendData('+value_key+', myDatas)\n';
  return code;
};

Blockly.Blocks['thingspeak_field'] = {
  init: function() {
    this.appendValueInput("field")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ts.gif", 40, 15, "*"))
        .appendField(new Blockly.FieldDropdown([["field1", "\"&field1=\""], ["field2", "\"&field2=\""], ["field3", "\"&field3=\""], ["field4", "\"&field4=\""], ["field5", "\"&field5=\""], ["field6", "\"&field6=\""], ["field7", "\"&field7=\""], ["field8", "\"&field8=\""]]), "field");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['thingspeak_field'] = function(block) {
  var dropdown_field = block.getFieldValue('field');
  var value_field = Blockly.Lua.valueToCode(block, 'field', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = '..'+dropdown_field +".."+ value_field +'..\n""';
  return code;
};
